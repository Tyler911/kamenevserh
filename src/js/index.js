// -----------------------------------------------------------------------------
// Deps
// -----------------------------------------------------------------------------

// global
import jQuery from 'js#/lib/jquery';
// styles
import 'sass#/style.scss';
// scripts
import { demo } from 'js#/modules/demo-module';

// -----------------------------------------------------------------------------
// Initialize
// -----------------------------------------------------------------------------

jQuery(function ($) {
	demo();
});

$(document).ready(function () {
	let firstTask = function () {
		let finalArr = {
			params: {},
			pagination: {}
		};

		$('body').on('change', '.js-filter-item', function (event) {
			let _this = $(this);
			let form = $('#filter')[0];
			let data = new FormData(form); // eslint-disable-line no-undef
			let newData = [...data.entries()];
			const params = {}
			const pagination = {}
			newData.forEach(function (el, index) {
				if (params.hasOwnProperty(el[0]) && !el[0].includes('sort_')) {
					params[el[0]].push(el[1]);
				} else if(el[1].length && !el[0].includes('sort_')){
					params[el[0]] = [el[1]];
				}else if (pagination.hasOwnProperty(el[0]) && el[0].includes('sort_')) {
					pagination[el[0]].push(Number(el[1]));
				} else if(el[1].length && el[0].includes('sort_')){
					pagination[el[0]] = Number(el[1]);
				}
			});


			finalArr.params = params
			finalArr.pagination = pagination
			const returnedArr = Object.assign(params, pagination);

			// вывод урл строки
			console.log(finalArr);

			let urlStr = '?';

			Object.entries(returnedArr).forEach(([key, value]) => {
				urlStr += `${key}=${value}&`;
			});

			urlStr = urlStr.substring(0, urlStr.length - 1);
			window.history.pushState('', document.title, urlStr);
		});
	}

	firstTask();
});
